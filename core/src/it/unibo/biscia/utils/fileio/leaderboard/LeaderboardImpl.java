package it.unibo.biscia.utils.fileio.leaderboard;

import it.unibo.biscia.core.Player;
import it.unibo.biscia.utils.fileio.FileIO;
import it.unibo.biscia.utils.fileio.FileIOImpl;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation of {@link LeaderboardIO}. This class uses {@link FileIO} to
 * write and read the setting's file.
 *
 */
public class LeaderboardImpl implements LeaderboardIO {

    private final FileIO fileIO;

    /**
     * Opens or creates in append mode the "leaderboard" file.
     * 
     * @see FileIO
     * @see FileIOImpl
     */
    public LeaderboardImpl() {
        this.fileIO = new FileIOImpl("Leaderboard");
    }

    @Override
    public final void update(final Player player) {
        final int currentScore = fileIO.getOrDefaultValue(player.getName(), Integer.class, 0).getSecond();
        if (currentScore < player.getPoints()) {
            fileIO.add(player.getName(), player.getPoints());
        }
        fileIO.build();
    }

    @Override
    public final Map<String, Integer> getScores() {
        return fileIO.getAllAs(Integer.class).entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).limit(10)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }

}
