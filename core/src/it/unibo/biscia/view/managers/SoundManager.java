package it.unibo.biscia.view.managers;

/**
 * Utility class for managing Sounds files.
 *
 */
public final class SoundManager {

    /**
     * Sound played when a Snake eats.
     */
    public static final Asset<Sound> EAT = new AssetImpl<>("sounds/Eat.wav", "eat", new Sound());

    /**
     * Sound played when a Snake dies.
     */
    public static final Asset<Sound> DIE = new AssetImpl<>("sounds/Die.wav", "die", new Sound());

    /**
     * A sound asset info.
     *
     */
    public static final class Sound {
        private Sound() {
        }
    }

    private SoundManager() {
    };

}
