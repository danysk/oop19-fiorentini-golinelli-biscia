package it.unibo.biscia.view.utils;

import it.unibo.biscia.core.Direction;
import it.unibo.biscia.core.Player;
import it.unibo.biscia.events.ActionObserver;
import it.unibo.biscia.events.ActionSubject;
import it.unibo.biscia.events.GenericEventSubject;

import com.badlogic.gdx.Input.Keys;

/**
 * Specialized class of {@link PlayerProcessorImpl} for {@link Player} "Two"
 * keyboard handle. This player fulfill its moves by keyboard's wasd keys.
 * 
 * @see Player
 * @see PlayerProcessor
 */
public class PlayerTwoProcessor extends PlayerProcessorImpl {

    /**
     * It creates a new Player Processor for player two.
     * 
     * @param player        the player
     * @param actionSubject the {@link ActionSubject} for notifying events.
     * 
     * @see PlayerProcessorImpl
     */
    public PlayerTwoProcessor(final Player player, final GenericEventSubject<ActionObserver> actionSubject) {
        super(player, actionSubject);
    }

    @Override
    public final boolean keyDown(final int keycode) {
        final GenericEventSubject<ActionObserver> subject = getSubject();
        switch (keycode) {
        case Keys.W:
            subject.notify(a -> a.move(getPlayer(), Direction.UP));
            break;
        case Keys.S:
            subject.notify(a -> a.move(getPlayer(), Direction.DOWN));
            break;
        case Keys.A:
            subject.notify(a -> a.move(getPlayer(), Direction.LEFT));
            break;
        case Keys.D:
            subject.notify(a -> a.move(getPlayer(), Direction.RIGHT));
            break;
        default:
            break;
        }
        return false;
    }

}
