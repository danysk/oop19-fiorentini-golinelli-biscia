package it.unibo.biscia.view.actors.ui;

import it.unibo.biscia.view.utils.Listenable;
import it.unibo.biscia.view.utils.Listener;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * It Creates an {@link IntOverLabel} that notifies its listeners when its state
 * is changed.
 * 
 *
 */
public class ListenableIntOverLabel extends IntOverLabel implements Listenable {

    private final Set<Listener> listeners;

    /**
     * Creates a ListenableIntOverLabel.
     * 
     * @param minValue    the minimum value of it's int state
     * @param maxValue    the maximum value of it's int state
     * @param initialData Initial int state
     * @param skin        the Skin of the label
     * 
     * @see IntOverLabel
     */
    public ListenableIntOverLabel(final int minValue, final int maxValue, final Integer initialData, final Skin skin) {
        super(minValue, maxValue, initialData, skin);
        this.listeners = new HashSet<>();
    }

    @Override
    public final void setPrevious() {
        super.setPrevious();
        this.update();
    }

    @Override
    public final void setNext() {
        super.setNext();
        this.update();
    }

    @Override
    public final void addListener(final Listener listener) {
        this.listeners.add(listener);
    }

    @Override
    public final void removeListener(final Listener listener) {
        this.listeners.add(listener);
    }

    @Override
    public final void update() {
        listeners.stream().forEach(Listener::stateChanged);
    }

}
