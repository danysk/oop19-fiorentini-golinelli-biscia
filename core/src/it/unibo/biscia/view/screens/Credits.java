package it.unibo.biscia.view.screens;

import it.unibo.biscia.view.actors.ui.ActionOverLabel;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Leaderboard screen.
 * 
 * @see AbstractScreenImpl
 * 
 *
 */
public class Credits extends AbstractScreenImpl {
    private static final int PADDING = 12;
    private final Table table;
    private final ActionOverLabel backLabel;

    public Credits() {
        this.table = new Table();
        this.backLabel = new ActionOverLabel("<- Back", getSkin(), () -> getBiscia().setScreen(new MainMenu()));
    }

    @Override
    public final void show() {
        this.table.setFillParent(true);
        this.table.add(new Label("Credits", getSkin())).pad(PADDING);
        this.table.row();
        this.table.add(new Label("</> with <3 by:", getSkin(), "disabled")).pad(PADDING);
        this.table.row();
        this.table.add(new Label("Giulio Golinelli:", getSkin())).padTop(PADDING);
        this.table.row();
        this.table.add(new Label("https://github.com/isthissuperuser", getSkin())).pad(PADDING);
        this.table.row();
        this.table.add(new Label("and", getSkin(), "disabled")).pad(PADDING);
        this.table.row();
        this.table.add(new Label("Piero Fiorentini", getSkin())).pad(PADDING);
        this.table.row();
        this.table.add(this.backLabel).pad(PADDING);
        this.getStage().addFocusableActor(backLabel);
        this.getStage().addActor(this.table);
        this.table.validate();
        this.getStage().setFocusedActor(backLabel);
    }

}
