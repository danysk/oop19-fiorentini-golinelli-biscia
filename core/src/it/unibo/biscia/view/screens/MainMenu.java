package it.unibo.biscia.view.screens;

import it.unibo.biscia.core.Controller;
import it.unibo.biscia.core.ControllerImpl;
import it.unibo.biscia.core.Player;
import it.unibo.biscia.events.ActionObserver;
import it.unibo.biscia.events.GenericEventSubject;
import it.unibo.biscia.events.StateObserver;
import it.unibo.biscia.utils.fileio.settings.SettingsIO;
import it.unibo.biscia.utils.fileio.settings.SettingsIOImpl;
import it.unibo.biscia.view.View;
import it.unibo.biscia.view.actors.ui.ActionOverLabel;
import it.unibo.biscia.view.managers.FontManager;
import it.unibo.biscia.view.utils.RandomColor;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

/**
 * The Main Menu Screen. It has an utility method {@link MainMenu#gameSetup()}
 * for synchronizing {@link View} and {@link Controller}.
 *
 */
public class MainMenu extends AbstractScreenImpl {
    private final Label logo;
    private final List<Label> buttons;
    private static final int PADDING = 6;
    private final Table table;

    /**
     * It creates the MainMenuScreen.
     */
    public MainMenu() {
        this.buttons = new ArrayList<>();
        final LabelStyle logoStyle = new LabelStyle();
        logoStyle.font = getManager().get(FontManager.LOGO.getName(), BitmapFont.class);
        logoStyle.fontColor = RandomColor.light();
        this.logo = new Label("Biscia", logoStyle);
        this.logo.setAlignment(Align.center);

        buttons.add(new ActionOverLabel("New Game", this.getSkin(), () -> this.gameSetup()));
        buttons.add(new Label("Continue", this.getSkin().get("disabled", LabelStyle.class)));
        buttons.add(new ActionOverLabel("Settings", this.getSkin(), () -> getBiscia().setScreen(new Settings())));
        buttons.add(new ActionOverLabel("Leaderboard", this.getSkin(), () -> getBiscia().setScreen(new Leaderboard())));
        buttons.add(new ActionOverLabel("Credits", this.getSkin(), () -> getBiscia().setScreen(new Credits())));
        buttons.add(new ActionOverLabel("Quit", this.getSkin(), () -> Gdx.app.exit()));
        this.table = new Table();
    }

    @Override
    public final void show() {
        this.table.setFillParent(true);
        this.table.add(this.logo).fill().pad(PADDING);
        this.table.row();
        for (int i = 0; i < buttons.size(); i++) {
            this.table.add(buttons.get(i)).pad(PADDING);
            this.table.row();
            if (i != 1) {
                this.getStage().addFocusableActor(buttons.get(i));
            }
        }
        this.getStage().addActor(this.table);
        this.table.validate();
        this.getStage().setFocusedActor(buttons.get(0));
        this.getStage().setEscapeActor(this.buttons.get(this.buttons.size() - 1));
    }

    /**
     * utility method for synchronizing {@link View} and {@link Controller} via
     * {@link GenericEventSubject} with their respective {@link StateObserver} and
     * {@link ActionObserver} as well as get {@link Player}s via {@link SettingsIO}.
     * This method is called by selecting the "new game" button. After all it calls
     * {@link Controller#start()}.
     * 
     */
    private void gameSetup() {
        final SettingsIO settingsIO = new SettingsIOImpl();
        final int numberOfPlayers = settingsIO.getNumberOfPlayers().getSecond();
        final String firstPlayer = settingsIO.getNamePlayer1().getSecond();
        final String secondPlayer = settingsIO.getNamePlayer2().getSecond();
        final it.unibo.biscia.view.screens.Game gameView = new it.unibo.biscia.view.screens.Game(
                settingsIO.getMusic().getSecond(), settingsIO.getSounds().getSecond());
        final List<String> players = new ArrayList<>();
        players.add(firstPlayer);
        if (numberOfPlayers == 2) {
            players.add(secondPlayer);
        }
        final Controller controller = new ControllerImpl(players, settingsIO.getInitialSpeed().getSecond(),
                settingsIO.getIncreasingSpeed().getSecond());
        controller.attach(gameView);
        gameView.attach(controller);
        gameView.setPlayers(controller.getPlayers());
        getBiscia().setScreen(gameView);
        controller.start();
    }
}
