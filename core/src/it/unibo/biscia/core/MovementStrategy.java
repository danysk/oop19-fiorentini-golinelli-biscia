package it.unibo.biscia.core;

import java.util.List;

@FunctionalInterface
interface MovementStrategy {

    /**
     * perform a movement on list of cells.
     * 
     * @param cells     list of cells to move
     * @param direction direction of movement
     * @return the new list of cells moved
     */
    List<SmartCell> move(List<SmartCell> cells, Direction direction);
}
