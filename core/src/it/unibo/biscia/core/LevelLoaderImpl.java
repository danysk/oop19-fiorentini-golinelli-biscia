package it.unibo.biscia.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.SerializationException;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

/**
 * for level generation.
 *
 */
class LevelLoaderImpl implements LevelLoader {

    private LevelManaged getEmptyLevel(final int cardinal) {
        final var ret = new LevelImpl(Level.MIN_COLS, Level.MIN_ROWS);
        ret.setCardinal(cardinal);
        return ret;
    }

    @Override
    public LevelManaged getFirstLevel() {
        return getLevel(1);
    }

    @Override
    public LevelManaged getLevel(final int cardinal) {
        LevelManaged ret;

        final FileHandle file = Gdx.files.internal("levels/" + "level" + cardinal + ".xml");
        if (file.exists()) {
            final Optional<LevelManaged> pop = populateFromFile(file);
            if (pop.isPresent()) {
                ret = pop.get();
                ret.setCardinal(cardinal);
                return ret;
            }
        }
        ret = getEmptyLevel(cardinal);
        final var entityFactory = new EntityFactoryImpl(ret);
        populateCasual(entityFactory, ret);
        return ret;

    }

    @Override
    public LevelManaged getNextLevel(final Level level) {
        return getLevel(level.getCardinal() + 1);
    }

    private void populateCasual(final EntityFactory entityFactory, final LevelManaged level) {

        if (level.getCardinal() == 1) {
            return;
        }
        level.addEntity(entityFactory.makeEdge());
        final Random random = new Random();
        int wallsCount = Levels.getEntityOfType(level, EntityType.WALL).size();
        while (wallsCount < level.getCardinal() * 2) {
            int cols;
            int rows;
            cols = random.nextInt(3) + 1;
            rows = random.nextInt(3) + 1;
            switch (random.nextInt(3)) {
            case 0:
                cols = 1;
                break;
            case 1:
                rows = 1;
                break;
            default:
                break;
            }

            final List<SmartCell> freeCells = Levels.getFreeCells(level);
            if (freeCells.isEmpty()) {
                break;
            }

            boolean ok = false;
            List<SmartCell> area = Arrays.asList(freeCells.get(random.nextInt(freeCells.size())));
            for (int i = 0; i < 10; i++) {
                area = level.getArea(freeCells.get(random.nextInt(freeCells.size())), cols, rows);
                if (Levels.entitiesOnCells(level, area).isEmpty()) {
                    ok = true;
                    break;
                }
            }
            if (!ok) {
                area = Arrays.asList(freeCells.get(random.nextInt(freeCells.size())));
            }

            level.addEntity(entityFactory.makeMovableWall(area,
                    Direction.values()[random.nextInt(Direction.values().length)],
                    MovementType.values()[random.nextInt(MovementType.values().length)], random.nextInt(3) + 1));
            wallsCount++;
        }
    }

    private Optional<LevelManaged> populateFromFile(final FileHandle file) {
        LevelManaged level;
        try {
            final Element root = new XmlReader().parse(file);

            if (!root.getName().equals("Level")) {
                return Optional.empty();
            }
            final int cols = this.getAttrbuteIntegerValue(root, "cols").orElse(Level.MIN_COLS);
            final int rows = this.getAttrbuteIntegerValue(root, "rows").orElse(Level.MIN_ROWS);
            level = new LevelImpl(cols, rows);
            final EntityFactory factory = new EntityFactoryImpl(level);
            if (this.getAttrbuteIntegerValue(root, "edge").orElse(0) == 1) {
                level.addEntity(factory.makeEdge());
            }
            List<EntityManaged> walls;
            walls = this.getWallNodes(factory, level, root);
            walls.forEach(w -> level.addEntity(w));

        } catch (SerializationException e) {
            e.printStackTrace();
            return Optional.empty();
        } catch (IllegalArgumentException e) {
            // xml file with not valid data
            return Optional.empty();
        }
        return Optional.of(level);
    }

    private List<EntityManaged> getWallNodes(final EntityFactory entityFactory, final LevelManaged level,
            final Element root) {

        final List<EntityManaged> ret = new LinkedList<>();
        for (final LevelLoader.WallType wallType : LevelLoader.WallType.values()) {
            final List<Element> walls = this.getChild(root,
                    wallType.name().substring(0, 1).toUpperCase(Locale.getDefault())
                            + wallType.name().substring(1).toLowerCase(Locale.getDefault()));

            for (final Element wall : walls) {
                List<SmartCell> cells = this.getCellsNodes(level, wall);
                if (wallType.equals(LevelLoader.WallType.AREA)) {
                    if (cells.size() != 2) {
                        throw new IllegalArgumentException("area need only 2 cells");
                    }
                    cells = level.getArea(cells.get(0), cells.get(1));

                }
                if (cells.isEmpty()) {
                    throw new IllegalArgumentException("no cell on wall");
                }
                final Optional<String> movement = this.getAttrbuteStringValue(wall, "movement");
                Optional<MovementType> movType = Optional.empty();
                if (movement.isPresent()) {
                    for (final var m : MovementType.values()) {
                        if (m.name().equalsIgnoreCase(movement.get())) {
                            movType = Optional.of(m);
                            break;
                        }
                    }
                }
                if (movType.isEmpty()) {
                    ret.add(entityFactory.makeWall(cells));
                } else {
                    final Optional<String> dir = this.getAttrbuteStringValue(wall, "direction");
                    Direction direction = Direction.RIGHT;
                    final int interval = this.getAttrbuteIntegerValue(wall, "interval").orElse(1);
                    if (dir.isEmpty()) {
                        if (cells.size() > 1) {
                            for (final var d : Direction.values()) {
                                if (level.getSideCell(cells.get(1), d).equals(cells.get(0))) {
                                    direction = d;
                                    break;
                                }
                            }
                        }
                    } else {
                        for (final var d : Direction.values()) {
                            if (d.name().equalsIgnoreCase(dir.get())) {
                                direction = d;
                                break;
                            }
                        }

                    }
                    ret.add(entityFactory.makeMovableWall(cells, direction, movType.get(), interval));
                }
            }
        }
        return ret;
    }

    private List<Element> getChild(final Element parent, final String name) {

        if (parent.getChildCount() == 0) {
            throw new IllegalArgumentException("no nodes on " + parent);
        }
        final List<Element> ret = new ArrayList<>(parent.getChildCount());

        for (int i = 0; i < parent.getChildCount(); i++) {
            final var node = parent.getChild(i);
            if (node.getName().equals(name)) {
                ret.add(parent.getChild(i));
            }
        }
        return ret;
    }

    private List<SmartCell> getCellsNodes(final LevelManaged level, final Element wall) {
        final var cells = wall.getChildrenByName("Cell");
        final List<SmartCell> ret = new ArrayList<>(cells.size);
        for (final var cell : cells) {
            final int col = this.getAttrbuteIntegerValue(cell, "col").get();
            final int row = this.getAttrbuteIntegerValue(cell, "row").get();
            ret.add(level.getCell(col, row));
        }
        return ret;
    }

    private Optional<Integer> getAttrbuteIntegerValue(final Element node, final String attributeName) {
        if (node.hasAttribute(attributeName)) {
            return Optional.of(node.getInt(attributeName));
        }
        return Optional.empty();
    }

    private Optional<String> getAttrbuteStringValue(final Element node, final String attributeName) {
        if (node.hasAttribute(attributeName)) {
            return Optional.of(node.getAttribute(attributeName));
        }
        return Optional.empty();
    }
}
