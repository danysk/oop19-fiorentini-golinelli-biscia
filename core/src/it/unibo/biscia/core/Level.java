package it.unibo.biscia.core;

import java.util.List;

/**
 * Level board of game, contain elements (wall, snake and food) over a simple
 * grid of cells.
 *
 */
public interface Level {
    /**
     * min number of columns.
     */
    int MIN_COLS = 80;
    /**
     * min number of rows.
     */
    int MIN_ROWS = 50;

    /**
     * the number of current level.
     * 
     * @return a integer
     */
    int getCardinal();

    /**
     * Getting a list of elements on board.
     * 
     * @return the entities contained on board
     */
    List<Entity> getEntities();

    /**
     * Get the numbers of columns on board.
     * 
     * @return a number positive
     */
    int getCols();

    /**
     * Get the numbers of rows on board.
     * 
     * @return a number positive
     */
    int getRows();

}
