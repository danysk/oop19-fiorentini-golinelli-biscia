package it.unibo.biscia.core;

/**
 * extend player with methods for setting points and lives.
 *
 */
interface PlayerManaged extends Player {
    /**
     * decrease lives and remove snake, is possible only if snake is set, also throw
     * IllegalStateException.
     */
    void dead();

    /**
     * increment points.
     * 
     * @param points points to add
     */
    void addPoints(int points);

    /**
     * set the snake to player. Is possible only if snake is not set and lives not
     * zero, also throw IllegalStateException.
     * 
     * @param directable the snake
     */
    void setDirectable(EntityManaged.Movable.Directable directable);

    /**
     * return the snake assigned to player.
     * 
     * @return the snake
     */
    EntityManaged.Movable.Directable getDirectable();

    /**
     * remove a directable inside the player.
     */
    void removeDirectable();
}
