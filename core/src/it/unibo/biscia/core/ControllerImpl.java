package it.unibo.biscia.core;

import it.unibo.biscia.events.ActionObserver;
import it.unibo.biscia.events.GenericEventSubject;
import it.unibo.biscia.events.GenericEventSubjectImpl;
import it.unibo.biscia.events.StateObserver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * a basic implementation of controller of game.
 *
 */
public final class ControllerImpl implements Controller, ActionObserver {
    private final GenericEventSubject<StateObserver> stateSubject = new GenericEventSubjectImpl<>();
    private Controller.Speed speed;
    private final boolean increaseSpeed;
    private volatile boolean pause = true;
    private boolean started;
    private boolean gameOver;
    private final List<PlayerManaged> players;
    private Timer timer;
    private final LevelLoader levelLoader;
    private LevelManaged level;
    private EntityFactory entityFactory;
    private int intFodEnergy;
    private final DirectionHarvester actHarv;
    private final Judge judge;
    private final InteractionManager interactionManager;
    private final Object sync = new Object();

    /**
     * make a new controller.
     * 
     * @param players       list of names of players, must be &gt; 0, uniqueness and
     *                      not empty or blank
     * @param initialSpeed  speed to start, from enum
     * @param increaseSpeed if speed increase between levels
     */
    public ControllerImpl(final List<String> players, final Speed initialSpeed, final boolean increaseSpeed) {
        Objects.requireNonNull(initialSpeed);
        Objects.requireNonNull(players);
        this.speed = initialSpeed;
        if (players.isEmpty()) {
            throw new IllegalArgumentException("no player's names passed");
        }
        if (players.size() > players.stream().map(s -> s.hashCode()).distinct().count()) {
            throw new IllegalArgumentException("not unique player's names");
        }
        if (players.stream().filter(s -> s.isEmpty() || s.isBlank()).count() > 0) {
            throw new IllegalArgumentException("absent player name");
        }
        this.players = new ArrayList<>(players.size());
        for (final String name : players) {
            this.players.add(new PlayerImpl(name));
        }
        this.actHarv = new DirectionHarvesterImpl();
        this.interactionManager = new InteractionManagerlImpl();
        this.judge = new JudgeImpl();
        this.increaseSpeed = increaseSpeed;
        this.levelLoader = new LevelLoaderImpl();
    }

    @Override
    public void end() {
        synchronized (this.sync) {
            this.pause = true;
            this.stopTimer();
        }
    }

    @Override
    public void move(final Player player, final Direction direction) {
        synchronized (this.sync) {
            // accept command only out of pause
            if (verifyIsStarted() && !this.pause) {
                if (!this.players.contains(player)) {
                    throw new IllegalArgumentException();
                }
                final var plm = this.players.get(this.players.indexOf(player));
                this.actHarv.setDirection(plm.getDirectable(), direction);
            }
        }
    }

    @Override
    public void pauseAndResume() {
        synchronized (this.sync) {
            if (verifyIsStarted()) {
                // reset eventuals commands harvested
                this.actHarv.getCommands();
                if (this.pause) {
                    this.pause = false;
                    this.stateSubject.notify(o -> o.gameResume());
                    this.startTimer();
                } else {
                    this.pause = true;
                    this.stopTimer();
                    this.stateSubject.notify(o -> o.gamePause());
                }
            }
        }
    }

    private void startTimer() {
        if (!Objects.isNull(this.timer) && !this.timer.stop) {
            this.timer.stopTimer();
        }
        int interval;

        if (this.speed.equals(Speed.STATIC)) {
            interval = (Timer.MAX_TIMER_INTERVAL - Timer.MIN_TIMER_INTERVAL) / 2 + Timer.MIN_TIMER_INTERVAL;
        } else {
            if (this.speed.equals(Speed.MAX)) {
                interval = Timer.MIN_TIMER_INTERVAL;
            } else {
                interval = Speed.values().length - this.speed.ordinal() - 1;
                interval = ((Timer.MAX_TIMER_INTERVAL - Timer.MIN_TIMER_INTERVAL) / (Speed.values().length - 1))
                        * interval + Timer.MIN_TIMER_INTERVAL;
            }
        }
        this.pause = false;
        this.timer = new ControllerImpl.Timer(interval);
        timer.start();
    }

    private void stopTimer() {
        this.pause = true;
        if (!Objects.isNull(this.timer)) {
            this.timer.stopTimer();
        }

    }

    @Override
    public void attach(final StateObserver observer) {
        stateSubject.attach(observer);
    }

    @Override
    public void detach(final StateObserver observer) {
        stateSubject.detach(observer);
    }

    @Override
    public List<Player> getPlayers() {
        return Collections.unmodifiableList(this.players.stream().map(p -> (Player) p).collect(Collectors.toList()));
    }

    private Set<EntityManaged> addSnakeAndFood() {
        final Set<EntityManaged> ret = new HashSet<>();
        this.players.forEach(p -> {
            if (Objects.isNull(p.getEntity()) && p.getLives() > 0) {

                p.setDirectable(this.entityFactory.makeBabySnake(!this.speed.equals(Speed.STATIC)));
                this.level.addEntity(p.getDirectable());
                ret.add(p.getDirectable());
            }
        });
        if (this.level.getEntities().stream().filter(e -> e.getType().equals(EntityType.FOOD)).findAny().isEmpty()) {
            this.intFodEnergy++;
            final var food = this.entityFactory.makeCasualFood(this.intFodEnergy);
            this.level.addEntity(food);
            ret.add(food);
        }
        return ret;
    }

    private Set<EntityManaged> prepareNewLevel(final LevelManaged level) {
        this.level = level;
        this.intFodEnergy = 0;
        this.entityFactory = new EntityFactoryImpl(this.level);
        this.players.forEach(p -> p.setDirectable(null));
        return addSnakeAndFood();
    }

    @Override
    public void start() {
        synchronized (this.sync) {
            if (this.started) {
                throw new IllegalStateException();
            }
            this.pause = true;
            this.started = true;
            this.prepareNewLevel(this.levelLoader.getFirstLevel());
            sendNewLevelMessage();
        }
    }

    private void sendEntityMessage(final Consumer<List<Entity>> event, final Set<Entity> entities) {
        if (!Objects.isNull(event) && !Objects.isNull(entities) && !entities.isEmpty()) {
            event.accept(Collections.unmodifiableList(entities.stream().collect(Collectors.toList())));
        }
    }

    private void sendEntityMessages(final Set<Entity> removed, final Set<Entity> updated, final Set<Entity> added) {
        this.sendEntityMessage(l -> this.stateSubject.notify(o -> o.remove(l)), removed);
        this.sendEntityMessage(l -> this.stateSubject.notify(o -> o.update(l)), updated);
        this.sendEntityMessage(l -> this.stateSubject.notify(o -> o.add(l)), added);
    }

    private void sendPlayerMessages(final Set<PlayerManaged> players) {
        if (!players.isEmpty()) {
            players.forEach(p -> {
                this.stateSubject.notify(o -> o.updatePlayer((Player) p));
            });
        }
    }

    private void sendNewLevelMessage() {
        this.stopTimer();
        this.stateSubject.notify(o -> o.newLevel((Level) this.level));
        sendPlayerMessages(this.players.stream().collect(Collectors.toSet()));
    }

    private void increaseSpeed() {
        if (this.increaseSpeed && this.speed != Controller.Speed.MAX) {
            this.speed = Controller.Speed.values()[this.speed.ordinal() + 1];
        }
    }

    private void clickTimer() {
        synchronized (this.sync) {
            // perform movement and detect interactions
            if (!interactionManager.performMovement(this.level, this.actHarv.getCommands())) {
                // nothing happened
                return;
            }
            // analyze interaction and progress of game
            this.judge.judges(this.intFodEnergy, this.level, this.players, interactionManager.getEat(),
                    interactionManager.getTrimmed(), interactionManager.getRemoved(), interactionManager.getUpdated());
            switch (judge.getState()) {
            case RESTART_LEVEL:
                this.stopTimer();
                this.prepareNewLevel(this.levelLoader.getLevel(this.level.getCardinal()));
                this.sendNewLevelMessage();
                break;
            case NEXT_LEVEL:
                this.stopTimer();
                this.increaseSpeed();
                sendEntityMessages(judge.getRemoved(), judge.getUpdated(), Collections.emptySet());
                this.prepareNewLevel(this.levelLoader.getNextLevel(this.level));
                this.sendNewLevelMessage();
                break;
            case GAMEOVER:
                this.gameOver = true;
                this.stopTimer();
                sendEntityMessages(Collections.emptySet(), judge.getUpdated(), Collections.emptySet());
                sendPlayerMessages(judge.getPlayersUpdates());
                this.stateSubject.notify(o -> o.gameOver());
                break;
            default:
                final Set<Entity> added = new HashSet<>();
                if (judge.addFood()) {
                    addSnakeAndFood().forEach(e -> added.add(e));
                }
                sendEntityMessages(judge.getRemoved(), judge.getUpdated(), added);
                sendPlayerMessages(judge.getPlayersUpdates());
                break;

            }
        }
    }

    private boolean verifyIsStarted() {
        return this.started && !this.gameOver;
    }

    private final class Timer extends Thread {
        private static final int MIN_TIMER_INTERVAL = 30;
        private static final int MAX_TIMER_INTERVAL = 100;
        private volatile boolean stop;
        private final int interval;

        Timer(final int interval) {
            if (interval < MIN_TIMER_INTERVAL) {
                this.interval = MIN_TIMER_INTERVAL;
            } else {
                if (interval > MAX_TIMER_INTERVAL) {
                    this.interval = MAX_TIMER_INTERVAL;
                } else {
                    this.interval = interval;
                }
            }
        }

        public void stopTimer() {
            this.stop = true;
        }

        @Override
        public void run() {
            while (!(this.isInterrupted() || this.stop)) {
                ControllerImpl.this.clickTimer();
                try {
                    sleep(this.interval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
