package it.unibo.biscia.core;

/**
 * is a player of game.
 *
 */
public interface Player {
    /**
     * the initial number of lives of a player.
     */
    int INITIAL_LIVES = 5;
    /**
     * point lost any dead.
     */
    int POINTS_FOR_DEAD = -1000;
    /**
     * point added for any energy point of food.
     */
    int POINTS_FOR_FOOD_ENERGY = 100;

    /**
     * the name.
     * 
     * @return the name of player
     */
    String getName();

    /**
     * actual points.
     * 
     * @return value of actual points
     */
    int getPoints();

    /**
     * remaining lives of player.
     * 
     * @return number of lives
     */
    int getLives();

    /**
     * get the entity moved from gamer.
     * 
     * @return the entity (usually snake) over control from gamer associated to a
     *         player
     */
    Entity getEntity();
}
