package it.unibo.biscia;

import it.unibo.biscia.view.managers.AssetManagerDecoratorImpl;
import it.unibo.biscia.view.screens.Loading;

import java.awt.Dimension;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

/**
 * This is the core crossplatform starting point of the application. Biscia is a
 * {@link Game} which implement and exposes all the support a game should need
 * like managing screens, refrishing the window, dispose the application and a
 * lot more.
 * 
 * @see Game
 *
 */
public final class Biscia extends Game {
    private final AssetManagerDecoratorImpl manager;

    /**
     * Utility class containing costants for the game.
     * 
     */
    public static final class Constants {
        private static Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        /**
         * The window title.
         */
        public static final String WINDOW_TITLE = "Biscia v: 1.0.0";

        /**
         * The window width.
         */
        public static final int WINDOW_WIDTH = (int) (dimension.width / 4 * 1.7);

        /**
         * The window height.
         */
        public static final int WINDOW_HEIGHT = (int) (dimension.height / 3 * 1.7);

        private Constants() {
        }
    }

    /**
     * It creates a new Biscia instance with its AssetManagerDecorator instance.
     */
    public Biscia() {
        this.manager = new AssetManagerDecoratorImpl();
    }

    @Override
    public void create() {
        Gdx.graphics.setContinuousRendering(false);
        // switch to loading screeen
        this.setScreen(new Loading());
    }

    @Override
    public void render() {
        // clears the screen
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        // call the screen render method
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        this.manager.dispose();
    }

    /**
     * get an asset manager instance.
     * 
     * @return the asset manager
     */
    public AssetManagerDecoratorImpl getAssetManager() {
        return manager;
    }
}
