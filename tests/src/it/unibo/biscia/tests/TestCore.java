package it.unibo.biscia.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import it.unibo.biscia.core.Controller;
import it.unibo.biscia.core.Controller.Speed;
import it.unibo.biscia.core.ControllerImpl;
import it.unibo.biscia.core.Direction;
import it.unibo.biscia.core.Entity;
import it.unibo.biscia.core.EntityType;
import it.unibo.biscia.core.Level;
import it.unibo.biscia.core.Player;
import it.unibo.biscia.events.StateObserver;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Test for public interfaces and classes of package it.unibo.biscia.core.
 *
 */
@RunWith(GdxTestRunner.class)
public class TestCore {

    /**
     * test if code generate error.
     * 
     * @param runnable operation to test errors
     * @param error    error to expect
     * @param messages detailed message
     */
    public static void checkErrors(final Runnable runnable, final Throwable error, final String messages) {
        try {
            runnable.run();
            fail("Expected error absent: " + messages);
        } catch (Throwable e) {
            if (e.getClass().isAssignableFrom(error.getClass())) {
                System.out.println(e.getClass() + " isAssignableFrom " + error.getClass());
            } else {
                fail("Unexpected error: " + e.getClass().getName() + " " + messages);
            }
        }
    }
    /**
     * test for controller.
     */
    @Test
    public void testController() {
        try {
            
            // test for initial parameters
            // names null
            // checkErrors(() -> new ControllerImpl(null, 0, false), new
            // NullPointerException(), "player names null");
            // no names
            checkErrors(() -> new ControllerImpl(Collections.emptyList(), Speed.STATIC, false),
                    new InvalidParameterException(), "player names empty");
            // not uniqueness name

            checkErrors(() -> new ControllerImpl(Arrays.asList("Biscia", "Biscia"), Speed.STATIC, false),
                    new InvalidParameterException(), "player names uniquenes");
            var names = Arrays.asList("Biscia", "Serpente", "Vipera");
            var controller = new ControllerImpl(names, Speed.SPEED1, false);
            // test for players
            var players = controller.getPlayers();
            //System.out.println(controller.getPlayers());
            assertEquals("number of players", names.size(), controller.getPlayers().size());
            for(int i = 0;i<controller.getPlayers().size(); i++) {
            	var player = controller.getPlayers().get(i);
            	assertEquals("Player name", player.getName(), names.get(i));
                assertEquals("initial player lives", Player.INITIAL_LIVES, player.getLives());
                assertEquals("not find one player", 1L,
                        names.stream().filter(name -> name.equals(player.getName())).count());
                assertEquals("initial player points", 0, player.getPoints());
                assertEquals("initial player lives", Player.INITIAL_LIVES, player.getLives());
            };
            names = Arrays.asList("Player1", "Player2");
            //speed static for exclude miss action on test for move
            controller =  new ControllerImpl(names, Speed.STATIC, false);
            players = controller.getPlayers();
            final var stateObs = new StateObserver() {
                private volatile boolean iniziato;
                private volatile boolean pause = true;
                private volatile boolean moved;
                private Level level;
                private List<String> names;
                private List<Player> players;
                private Controller controller;
                public void move(final Player player, final Direction direction) {
                	boolean resume = false;
                	if (!this.inPause()) {
                		controller.pauseAndResume();
                        while (!pause) {
                        }
                		resume = true;
                	}
                    int col = player.getEntity().getCells().get(0).getCol();
                    int row = player.getEntity().getCells().get(0).getRow();
                    //System.out.println("Move " + player + " head col " + col + " row " + row + " direction " + direction);
                    
                    int col2 = col;
                    int row2 = row;
                    switch (direction) {
					case UP:
						row2--;
						break;
					case DOWN:
						row2++;
						break;
					case LEFT:
						col2--;
						break;
					case RIGHT:
						col2++;
						break;
					default:
						break;
					}
                    if (row2<0) row2=level.getRows()-1;
                    if (row2>=level.getRows()) row2=0;
                    if (col2<0) col2=level.getCols()-1;
                    if (col2>=level.getCols()) col2=0;
               		controller.pauseAndResume();
                    while (pause) {
                    }
                    moved=false;
               		controller.move(player, direction);
                    while (!moved) {
                    }
                    controller.pauseAndResume();
                    while (!pause) {
                    }
                    //verify movement of head  of snake
                    //System.out.println("New col expected " + col2 + " received " + player.getEntity().getCells().get(0).getCol());
                    //System.out.println("New row expected " + row2 + " received " + player.getEntity().getCells().get(0).getRow());
                    assertEquals("New col unexpected",col2,player.getEntity().getCells().get(0).getCol() );
                    assertEquals("New row unexpected",row2,player.getEntity().getCells().get(0).getRow() );
                    controller.pauseAndResume();
                	if (resume) {
                		controller.pauseAndResume();
                	}
                }
                
                public void setController(final Controller controller) {
                	this.controller = controller;
                }
                public void setNames(final List<String> names) {
                	this.names = names;
                }
                public void setPLayers(final List<Player> players) {
                	this.players = players;
                }
                
                public boolean inPause() {
                    return this.pause;
                }

                private void CheckLevel(final String event) {
                    assertTrue("Not food on " + event,this.level.getEntities().stream().filter(e -> e.getType().equals(EntityType.FOOD)).findAny().isPresent());
                    assertEquals("number of players on "+ event, names.size(), players.size());
                    for(var player: players) {
                        assertTrue("Not retrived entity for player " + player.getName() + " on " + event,this.level.getEntities().stream().filter(e -> e.equals(player.getEntity())).findAny().isPresent());
                    }
                }
                
                @Override
                public void update(final List<Entity> entities) {
                    //System.out.println("test update " + entities);
                    assertFalse("update entity in pause", this.pause);
                    CheckLevel("update");
                    this.moved = true;
                }

                @Override
                public void gameResume() {
                    //System.out.println("test gameResume");
                    assertTrue("resume not started", this.iniziato);
                    assertTrue("resume not in pause", this.pause);
                    this.pause = false;
                }

                @Override
                public void remove(final List<Entity> entities) {
                    //System.out.println("test remove " + entities);
                    assertFalse("remove entity in pause", this.pause);
                    CheckLevel("remove");
                }

                @Override
                public void gamePause() {
                    //System.out.println("gamePause");
                    if (this.iniziato) {
                        assertFalse("pause in pause", this.pause);
                    }
                    this.pause = true;
                }

                @Override
                public void newLevel(final Level level) {
                    //System.out.println("newLevel " + level);
                    this.pause = true;
                    this.iniziato = true;
                    this.level = level;
                    CheckLevel("newLevel");
                }

                @Override
                public void gameOver() {
                    System.out.println("gameOve");
                    this.pause = true;
                }

                @Override
                public void add(final List<Entity> entities) {
                    //System.out.println("test add " + entities + " Level=" + this.level);
                    CheckLevel("add");
                }

                @Override
                public void updatePlayer(final Player player) {
                    //System.out.println("test updatePlayer " + player + " players=" + players);
                    CheckLevel("updatePlayer");
                }
            };
            
            stateObs.setNames(names);
            stateObs.setPLayers(players);
            stateObs.setController(controller);
            controller.attach(stateObs);
            controller.start();
            controller.pauseAndResume();
            assertFalse("Resume fail", stateObs.inPause());
            controller.pauseAndResume();
            assertTrue("Pause fail", stateObs.inPause());
            controller.pauseAndResume();
            for (int i=0;i<10;i++) {
		        stateObs.move(players.get(0), Direction.UP);
		        stateObs.move(players.get(1), Direction.UP);
		        stateObs.move(players.get(0), Direction.LEFT);
		        stateObs.move(players.get(1), Direction.LEFT);
		        stateObs.move(players.get(0), Direction.DOWN);
		        stateObs.move(players.get(1), Direction.DOWN);
		        stateObs.move(players.get(0), Direction.RIGHT);
		        stateObs.move(players.get(1), Direction.RIGHT);
            }
	        stateObs.move(players.get(0), Direction.DOWN);
	        stateObs.move(players.get(1), Direction.DOWN);
            //final int sleepXSec = 10_000;
            //Thread.sleep(sleepXSec);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.toString());
        }
    }
}
